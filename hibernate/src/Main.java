import liwey.keystat.entity.*;
import lombok.extern.log4j.Log4j;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.hibernate.cfg.Configuration;

import javax.persistence.metamodel.EntityType;

@Log4j
public class Main {
    private static final SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

    public static Session getSession() throws HibernateException {
        return sessionFactory.openSession();
    }

    public static void main(final String[] args) throws Exception {
        final Session session = getSession();
        try {
            Trades trades2 = new Trades();
            trades2.setName("xxx");
            Transaction txn = session.beginTransaction();
            session.createQuery("delete Trades2 where name = ?").setParameter(0,"xxx").executeUpdate();
            session.save(trades2);
            txn.commit();

//            log.info("querying all the managed entities...");
//            final Metamodel metamodel = session.getSessionFactory().getMetamodel();
//            for (EntityType<?> entityType : metamodel.getEntities()) {
//                final String entityName = entityType.getName();
//                final Query query = session.createQuery("from " + entityName);
//                log.info("executing: " + query.getQueryString());
//                for (Object o : query.list()) {
//                    log.info("  " + o);
//                }
//            }
        } finally {
            session.close();
            sessionFactory.close();
            log.info("Done.");
        }
    }
}