/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/11 14:39
 */


import java.io.File;
import java.io.IOException;
//
import lombok.extern.log4j.Log4j;
import org.apache.hadoop.conf.Configuration;
//
import org.apache.log4j.Logger;

/**
 * ConfigurationManager create a Hadoop Configuration object
 *
 *
 * @author Mahmoud Parsian
 *
 */
@Log4j
public class ConfigurationManager {

    static String HADOOP_HOME = "D:/scratch/llian/apps/hadoop-2.7.3";
    static String HADOOP_CONF_DIR = "D:/scratch/llian/apps/hadoop-2.7.3/etc/hadoop";

    // identify the cluster: note that we may define many clusters
    // and submit it to different clusters based on parameters and conditions
    //
    static final String HADOOP_CONF_CORE_SITE = ConfigurationManager.getHadoopConfDir() + "/core-site.xml";
    static final String HADOOP_CONF_HDFS_SITE = ConfigurationManager.getHadoopConfDir() + "/hdfs-site.xml";
    static final String HADOOP_CONF_MAPRED_SITE = ConfigurationManager.getHadoopConfDir() + "/mapred-site.xml";
    static final String HADOOP_CONF_YARN_SITE = ConfigurationManager.getHadoopConfDir() + "/yarn-site.xml";

    //
    public static void setHadoopHomeDir(String dir) {
        HADOOP_HOME = dir;
    }

    public static String getHadoopHomeDir() {
        return HADOOP_HOME;
    }

    //
    public static void setHadoopConfDir(String dir) {
        HADOOP_CONF_DIR = dir;
    }

    public static String getHadoopConfDir() {
        return HADOOP_CONF_DIR;
    }

    static Configuration createConfiguration() throws IOException {
        log.info("createConfiguration() started.");
        log.info("createConfiguration() HADOOP_HOME=" + ConfigurationManager.getHadoopHomeDir());
        log.info("createConfiguration() HADOOP_CONF_DIR=" + ConfigurationManager.getHadoopConfDir());

        Configuration config = new Configuration();

        config.addResource(new File(HADOOP_CONF_CORE_SITE).getAbsoluteFile().toURI().toURL());   
        config.addResource(new File(HADOOP_CONF_HDFS_SITE).getAbsoluteFile().toURI().toURL());   
        config.addResource(new File(HADOOP_CONF_MAPRED_SITE).getAbsoluteFile().toURI().toURL()); 
        config.addResource(new File(HADOOP_CONF_YARN_SITE).getAbsoluteFile().toURI().toURL());   

        config.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName()); 
        config.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());         
        config.set("hadoop.home.dir", ConfigurationManager.getHadoopHomeDir());
        config.set("hadoop.conf.dir", ConfigurationManager.getHadoopConfDir());
        config.set("yarn.conf.dir", ConfigurationManager.getHadoopConfDir());

        log.info("createConfiguration(): Configuration created.");
        return config;
    }

}
