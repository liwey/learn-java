/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/22 0:15
 */

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 */
public class Main {
    public static void main(String args[]) {
        try {
            ServerAddress serverAddress = new ServerAddress("localhost", 27017);
            List<ServerAddress> seeds = new ArrayList<ServerAddress>();
            seeds.add(serverAddress);

            //通过连接认证获取MongoDB连接
            //MongoCredential credential = MongoCredential.createScramSha1Credential("admin", "test", "welcome1"
            //        .toCharArray());
            MongoClient mongoClient = new MongoClient(seeds, MongoClientOptions.builder().build());

            //连接到数据库
            MongoDatabase db = mongoClient.getDatabase("test");
            System.out.println("Connect to database successfully");

            MongoCollection<Document> col1 = db.getCollection("people");
            HashMap<String, Object> p1 = new HashMap<>();
            p1.put("id", "1");
            p1.put("firstName", "abc");
            p1.put("lastName", "bbb");
            Document d1 = new Document("p1", p1);
            col1.insertOne(d1);

            MongoCollection<Document> col = db.getCollection("col");
            System.out.println("集合col选择成功");
            //插入文档
            /**
             * 1. 创建文档 org.bson.Document 参数为key-value的格式
             * 2. 创建文档集合List<Document>
             * 3. 将文档集合插入数据库集合中 mongoCollection.insertMany(List<Document>) 插入单个文档可以用 mongoCollection.insertOne(Document)
             * */
            Document child = new Document("title", "inner").append("f2", 123);
            HashMap<String, Object> obs = new HashMap<>();
            obs.put("a", 1);
            obs.put("b", "abc");
            obs.put("c", 1.23);
            obs.put("d", new Date());

            Document document = new Document("title", "MongoDB")
                    .append("description", "database")
                    .append("map", obs)
                    .append("likes", 100)
                    .append("by", "Fly")
                    .append("f1", 1000)
                    .append("created", new Date())
                    .append("child", child);

            List<Document> documents = new ArrayList<Document>();
            documents.add(document);
            col.insertMany(documents);
            System.out.println("文档插入成功" + col.count());

            MongoCursor<Document> mongoCursor = col.find().iterator();
            while (mongoCursor.hasNext()) {
                System.out.println(mongoCursor.next());
            }
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }
}
