import feign.Feign;
import feign.gson.GsonDecoder;
import spring.cloud.feign.GitHub;
import spring.cloud.feign.JobServer;
import spring.cloud.feign.ServiceHi;

import java.util.List;

public class FeignTest // extends TestCase
{
    public void testHi(){
        ServiceHi hi = Feign.builder().target(ServiceHi.class, "http://localhost:8764");
        System.out.println(hi.hello("xyz"));
    }

    public void testGithub() throws Throwable {
        GitHub github = Feign.builder().decoder(new GsonDecoder()).target(GitHub.class, "https://api.github.com");
        // Fetch and print a list of the contributors to this library.
        List<spring.cloud.feign.FeignTest.Contributor> contributors = github.contributors("OpenFeign", "feign");
        for (spring.cloud.feign.FeignTest.Contributor contributor : contributors) {
            System.out.println(contributor.getLogin() + " (" + contributor.getContributions() + ")");
        }
    }

    public void testJobServer(){
        JobServer jobServer = Feign.builder().target(JobServer.class,"http://slc09woc:8090");
        System.out.println(jobServer.getJobs());
    }
}
