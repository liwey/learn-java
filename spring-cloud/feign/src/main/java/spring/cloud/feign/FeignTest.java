package spring.cloud.feign;

import feign.Feign;
import feign.gson.GsonDecoder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class FeignTest {
    @Data
    public static class Contributor {
        String login;
        int contributions;
    }

    public static void main(String... args) {
        GitHub github = Feign.builder().decoder(new GsonDecoder()).target(GitHub.class, "https://api.github.com");
        // Fetch and print a list of the contributors to this library.
        List<Contributor> contributors = github.contributors("liweys", "guava");
        for (Contributor contributor : contributors) {
            System.out.println(contributor.login + " (" + contributor.contributions + ")");
        }
    }
}

