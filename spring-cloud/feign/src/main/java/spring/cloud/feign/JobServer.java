package spring.cloud.feign;

import feign.Param;
import feign.RequestLine;

/**
 * <p></p>
 *
 * @author Leon Zeng
 * @version 1.0
 * @date 2018/3/25 21:35
 */
public interface JobServer {
    @RequestLine("GET /jobs")
    String getJobs();
}
