package spring.cloud.feign;

import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;

import java.util.List;

/**
 * <p></p>
 *
 * @author Leon Zeng
 * @version 1.0
 * @date 2018/3/25 17:36
 */
@FeignClient(value = "service-hi",fallback = ServiceHiHystric.class)
public interface ServiceHi {
    @RequestLine("GET /hi?name={name}")
    String hello(@Param("name") String name);
}
