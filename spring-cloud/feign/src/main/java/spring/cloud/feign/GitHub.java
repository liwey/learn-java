package spring.cloud.feign;

import feign.Param;
import feign.RequestLine;

import java.util.List;

public interface GitHub {
    // RequestLine注解声明请求方法和请求地址,可以允许有查询参数
    @RequestLine("GET /repos/{owner}/{repo}/contributors")
    List<FeignTest.Contributor> contributors(@Param("owner") String owner, @Param("repo") String repo);
}
