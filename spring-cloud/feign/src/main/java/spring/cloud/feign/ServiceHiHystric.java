/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/12 11:01
 */

package spring.cloud.feign;

import feign.Param;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class ServiceHiHystric implements ServiceHi {
    @Override
    public String hello(String name) {
        return "sorry "+name;
    }
}

