/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/15 11:00
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import junit.framework.TestCase;

import java.math.BigInteger;
import java.text.ParseException;

/**
 *
 */
public class NumTest extends TestCase {
    public void testNumber() {
        Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();
        Object[] numbers1 = new Object[]{1, 1.0, 1.23, 9223372036854775807L, new BigInteger("9223372036854775808"),
                1.23E10, 1.23E20, Double.NaN, Double.POSITIVE_INFINITY};
        String json = gson.toJson(numbers1);
        System.out.println(json);
        Object[] numbers2 = gson.fromJson(json, Object[].class);
        System.out.println(gson.toJson(numbers2));
        for (int i = 0; i < numbers2.length; i++) {
            System.out.println(numbers1[i] + " (" + numbers1[i].getClass().toString().substring(6)
                    + ")\t-->\t" + numbers2[i] + " (" + numbers2[i].getClass().toString().substring(6) + ")");
        }
    }
}
