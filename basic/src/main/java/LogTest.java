import lombok.extern.log4j.Log4j2;

/**
 * @author Leon Zeng
 * @version 1.0
 * @since 2018/7/23 14:29
 */
@Log4j2
public class LogTest {
    public static void main(String[] args) {
        int n = 1000000;

        long t1 = System.nanoTime();
        for(int i = 0; i < n; i++) {
            log.error("something");
        }
        long t2 = System.nanoTime();
        System.out.println((t2-t1)/n);
    }
}
