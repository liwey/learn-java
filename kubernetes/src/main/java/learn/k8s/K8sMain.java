/* Copyright (c) 2018 Oracle and/or its affiliates. All rights reserved.*/

package oracle.insight.spark;

import com.google.gson.reflect.TypeToken;
import io.kubernetes.client.ApiClient;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.models.V1Namespace;
import io.kubernetes.client.models.V1ObjectMeta;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1PodList;
import io.kubernetes.client.util.Config;
import io.kubernetes.client.util.Watch;

import java.io.IOException;

/**
 * @author Leon Zeng
 * @version 1.0
 * @see <a href="https://github.com/kubernetes-client/java">Doc</a>
 * @since 2018/8/1 16:44
 */
public class K8sMain {
    public static void main(String[] args) throws ApiException {
        ApiClient client = Config.fromUrl("https://slc09woc.us.oracle.com:6443", false);
        //ApiClient client = Config.fromUrl("http://slc09woc.us.oracle.com:8001", false);
        //client.setApiKey("68xhve.0avtq9e3exa8jedb");
        client.setConnectTimeout(300000);
        client.setUsername("spark");
        client.setPassword("welcome1");
        //client.setApiKey("eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6InNwYXJrLXRva2VuLTJ0OGw5Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6InNwYXJrIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiM2M4M2EyY2EtNWNkYS0xMWU4LTg0YWUtMDAyMWY2ZWFmOWQxIiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50OmRlZmF1bHQ6c3BhcmsifQ.gPqHR3CiVBk-mIQFU4Dis0cw9VeTN1sMVhxRtb9G88kTa3xr7BDEeCl8Ws9r9uxG16sriYUwTHuKsyA2qHh3Y1vhdRHhABFyyGYloF1wcIY25Irm-atJjIY3M4S6h7ZbDaBDSxCCif4Qo08euCiySdiF4UND15CDUMOAmQ9MhDRdWyHmFPqRkc_UOcRBudPZst2Re1IpOa-gw9-FnbtBGYLpyy_WA2ktwsB43Nor7wPstVG6s6_0DewKivGEKc05kcrqJQ8PszcG3aOfFJsZ84wDynbvMKEyinMOqx2oVoi9rJWL4Xpj2eckfEIIDmewusjASUjXWdVc92WAPqu5FA");
        Configuration.setDefaultApiClient(client);

        CoreV1Api api = new CoreV1Api();
        //V1PodList list = api.listPodForAllNamespaces(null, null, null, "spark-role=driver", null, null, null, null, null);
        V1PodList list = api.listNamespacedPod("default",null, null, null, null, null, null, null, null, null);
        for (V1Pod item : list.getItems()) {
            V1ObjectMeta meta = item.getMetadata();
            System.out.printf("name=%s, status=%s, created=%s\n", meta.getName(), item.getStatus().getPhase(), item.getStatus().getStartTime());
        }

        //Watch<V1Namespace> watch = Watch.createWatch(
        //        client,
        //        api.listNamespaceCall(null, null, null, null, null, 5, null, null, Boolean.TRUE, null, null),
        //        new TypeToken<Watch.Response<V1Namespace>>() {
        //        }.getType());
        //
        //for (Watch.Response<V1Namespace> item : watch) {
        //    System.out.printf("%s : %s%n", item.type, item.object.getMetadata().getName());
        //}
    }
}