/**
 *
 */
package oracle.insight.monitor

data class MonitorContext(val interval: Int) {
    // monitor
    var monitorInterval = interval
    var monitorMaxRetries = 100
    var monitorEmails = ""

    // rest server
    var serverAddress = "0.0.0.0"
    var serverPort = 8091
    var serverMaxConnections = 10
    var serverAuth = BasicAuth("admin", "welcome1")

    // jdbc of job server db
    var jdbcDriver = "org.h2.Driver"
    var jdbcUrl = "jdbc:h2:tcp://jobserver:19092/h2-db"
    var jdbcAuth: BasicAuth? = null

    // job server
    var jobServerUrl = "http://jobserver:8090"
    var jobServerAuth: BasicAuth? = null

    // proxy
    var proxy: HttpProxy? = null

    // YARN
    var yarnUrl = "http://yarn-rm:8088/ws/v1/cluster"
    var submitThresholdMb: Long = 2304
    var yarnAuth: BasicAuth? = null
}

class HttpProxy {

}

class BasicAuth(s: String, s1: String) {
    val user: String = s
    var password: String = s1
}

data class Context(val a:Int, val b: Int,var c:String)
