import java.util.*

/**
 *
 */
object DataProviderManager {

    val allDataProviders: Collection<String>
        get() = Collections.emptyList()
}

class Site {
    var name = "菜鸟教程"

    class DeskTop{
        var url = "www.runoob.com"
        fun showName(){
            print{"desk legs $"} // 错误，不能访问到外部类的方法和变量
        }
    }
}