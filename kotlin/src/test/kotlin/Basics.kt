import junit.framework.TestCase

/**
 *
 */

class BasicsTest : TestCase() {
    fun testTrim() {
        val s1 = """
    |多行字符串
    |菜鸟教程
    |多行字符串
    |Runoob
    """
        val s2 = s1.trimMargin()
        println(s1)
        println(s2)    // 前置空格删除了
    }

    fun testArray() {
        //[1,2,3]
        val a = arrayOf(1, 2, 3)
        //[0,2,4]
        val b = Array(3, { i -> (i * 2) })

        //读取数组内容
        println(a[0])    // 输出结果：1
        println(b[1])    // 输出结果：2

        for ((index, value) in b.withIndex()) {
            println("the element at $index is $value")
        }
    }

    fun testBoxing() {
        val a: Int = 100

        //经过了装箱，创建了两个不同的对象
        val boxedA: Int? = a
        val anotherBoxedA: Int? = a

        //虽然经过了装箱，但是值是相等的，都是100
        println(boxedA === anotherBoxedA) //  true，值相等，128 之前对象地址一样
        println(boxedA == anotherBoxedA) // true，值相等
    }

    fun testStr2() {
        val price = "${'$'}9.99"
        println(price)  // 求值结果为 $9.99
    }

    fun testIf() {
        val a = 1
        val b = 2
        println(max(a, b))
    }

    private fun max(a: Int, b: Int): Int {
        return if (a > b) {
            print("aaa")
            a
        } else {
            print("bbb")
            b
        }
    }

    fun testWhen() {
        check(1)
        check(100)
        check("abc")
    }

    private fun check(x: Any) {
        when (x) {
            in 1..10 -> println("x is in the range")
            in 100..1000 -> println("x is valid")
        //!in 10..20 -> println("x is outside the range")
            else -> println("none of the above")
        }
    }

    fun hasPrefix(x: Any) = when (x) {
        is String -> x.startsWith("xxx")
        else -> false
    }

    fun testWhen2() {
        println(hasPrefix("xxx"))
        println(hasPrefix("xcc"))
        println(hasPrefix(1))
    }

    fun testWhenIn() {
        val items = setOf("apple", "banana", "kiwi")
        when {
            "orange" in items -> println("juicy")
            "apple" in items -> println("apple is fine too")
        }
    }

    fun testLabel() {
        val a = arrayOf(1, 2, 3)
        val b = arrayOf("a", "b", "c")

        a.forEach there@{
            b.forEach {
                if (it == "a")
                    return@forEach
                print("$it, ")
            }
            if (it == 2)
                return@there
            println("--$it")
        }
    }
}