/**
 *
 */

interface MyInterface {
    var name:String //name 属性, 抽象的
    fun bar()
    fun foo() {
        // 可选的方法体
        println("foo")
    }
}

interface MyInterface2 {
    var name2:String //name 属性, 抽象的
    fun bar2()
    fun foo2() {
        // 可选的方法体
        println("foo")
    }
}


class Child2 : MyInterface, MyInterface2 {
    override fun bar2() {

    }

    override var name: String = "my1" //重载属性
    override var name2: String = "my2"

    override fun bar() {
        // 方法体
        println("bar")
    }
}
fun main(args: Array<String>) {
    val c =  Child2()
    c.foo();
    c.bar();
    println(c.name + c.name2)

}