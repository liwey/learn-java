import junit.framework.TestCase

/**
 *
 */

class Runoob(name: String) {  // 类名为 Runoob
    // 大括号内是类体构成
    var url: String = "http://www.runoob.com"
    var country: String = "CN"
    var siteName: String
    var alexa: Int = 0

    init {
        siteName = name
        println("初始化网站名: ${name}")
    }

    // 次构造函数
    constructor (name: String, alexa: Int) : this(name) {
        this.siteName = name
        this.alexa = alexa
    }

    fun printTest() {
        println("Alexa 排名 $alexa of $country")
    }
}

class tests : TestCase() {
    fun test1() {
        val a = Runoob("a")
        a.country = "bb"
        a.printTest()
    }
}

open class Base {
    open fun f(){}
}

abstract class Derived : Base() {
    override abstract fun f()
}

class Child : Derived() {
    override fun f() {

    }
}

class Outer {                  // 外部类
    private val bar: Int = 1
    class Nested {             // 嵌套类
        fun foo() = 2
    }
}