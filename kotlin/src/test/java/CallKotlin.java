/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/3 15:59
 */

import com.google.gson.Gson;
import junit.framework.TestCase;
import oracle.insight.monitor.Context;
import oracle.insight.monitor.MonitorContext;

/**
 *
 */
public class CallKotlin extends TestCase{
    public void testPojo(){
        Pojo1 pojo1 = new Pojo1();
        pojo1.setA(1);
        pojo1.setB("abc");
        pojo1.setB(null);
        System.out.println(pojo1);
    }

    public void testNested(){
        Outer.Nested n = new Outer.Nested();
        n.foo();
    }

    public void testContext(){
        MonitorContext context = new MonitorContext(1000);
        System.out.println(context.toString());
        context.component1();

        Context context1 = new Context(1,2,"abc");
        System.out.println(context1.toString());
    }

    public void testSingleton(){
        DataProviderManager.INSTANCE.getAllDataProviders();
    }
}
