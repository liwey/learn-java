/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/12 12:12
 */


import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.List;

/**
 * see details at https://square.github.io/retrofit/
 @GET("group/{id}/users")
 Call<List<User>> groupList(@Path("id") int groupId, @Query("sort") String sort);

 @GET("group/{id}/users")
 Call<List<User>> groupList(@Path("id") int groupId, @QueryMap Map<String, String> options);

 */
public class RetrofitMain{
    public static void main(String[] args) throws IOException {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        GitHubService service = retrofit.create(GitHubService.class);
        Call<List<Repo>> repos = service.listRepos("zenglian");
    }
}


