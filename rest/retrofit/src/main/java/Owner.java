import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Owner {

    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("events_url")
    private String eventsUrl;
    @SerializedName("followers_url")
    private String followersUrl;
    @SerializedName("following_url")
    private String followingUrl;
    @SerializedName("gists_url")
    private String gistsUrl;
    @SerializedName("gravatar_id")
    private String gravatarId;
    @SerializedName("html_url")
    private String htmlUrl;
    private Long id;
    private String login;
    @SerializedName("organizations_url")
    private String organizationsUrl;
    @SerializedName("received_events_url")
    private String receivedEventsUrl;
    @SerializedName("repos_url")
    private String reposUrl;
    @SerializedName("site_admin")
    private Boolean siteAdmin;
    @SerializedName("starred_url")
    private String starredUrl;
    @SerializedName("subscriptions_url")
    private String subscriptionsUrl;
    private String type;
    private String url;

}
