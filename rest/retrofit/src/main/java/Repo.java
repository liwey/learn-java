import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Repo {
    @SerializedName("archive_url")
    private String archiveUrl;
    private Boolean archived;
    @SerializedName("assignees_url")
    private String assigneesUrl;
    @SerializedName("blobs_url")
    private String blobsUrl;
    @SerializedName("branches_url")
    private String branchesUrl;
    @SerializedName("clone_url")
    private String cloneUrl;
    @SerializedName("collaborators_url")
    private String collaboratorsUrl;
    @SerializedName("comments_url")
    private String commentsUrl;
    @SerializedName("commits_url")
    private String commitsUrl;
    @SerializedName("compare_url")
    private String compareUrl;
    @SerializedName("contents_url")
    private String contentsUrl;
    @SerializedName("contributors_url")
    private String contributorsUrl;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("default_branch")
    private String defaultBranch;
    @SerializedName("deployments_url")
    private String deploymentsUrl;
    private String description;
    @SerializedName("downloads_url")
    private String downloadsUrl;
    @SerializedName("events_url")
    private String eventsUrl;
    private Boolean fork;
    private Long forks;
    @SerializedName("forks_count")
    private Long forksCount;
    @SerializedName("forks_url")
    private String forksUrl;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("git_commits_url")
    private String gitCommitsUrl;
    @SerializedName("git_refs_url")
    private String gitRefsUrl;
    @SerializedName("git_tags_url")
    private String gitTagsUrl;
    @SerializedName("git_url")
    private String gitUrl;
    @SerializedName("has_downloads")
    private Boolean hasDownloads;
    @SerializedName("has_issues")
    private Boolean hasIssues;
    @SerializedName("has_pages")
    private Boolean hasPages;
    @SerializedName("has_projects")
    private Boolean hasProjects;
    @SerializedName("has_wiki")
    private Boolean hasWiki;
    private String homepage;
    @SerializedName("hooks_url")
    private String hooksUrl;
    @SerializedName("html_url")
    private String htmlUrl;
    private Long id;
    @SerializedName("issue_comment_url")
    private String issueCommentUrl;
    @SerializedName("issue_events_url")
    private String issueEventsUrl;
    @SerializedName("issues_url")
    private String issuesUrl;
    @SerializedName("keys_url")
    private String keysUrl;
    @SerializedName("labels_url")
    private String labelsUrl;
    private String language;
    @SerializedName("languages_url")
    private String languagesUrl;
    private Object license;
    @SerializedName("merges_url")
    private String mergesUrl;
    @SerializedName("milestones_url")
    private String milestonesUrl;
    @SerializedName("mirror_url")
    private Object mirrorUrl;
    private String name;
    @SerializedName("notifications_url")
    private String notificationsUrl;
    @SerializedName("open_issues")
    private Long openIssues;
    @SerializedName("open_issues_count")
    private Long openIssuesCount;
    private Owner owner;
    @SerializedName("private")
    private Boolean _private;
    @SerializedName("pulls_url")
    private String pullsUrl;
    @SerializedName("pushed_at")
    private String pushedAt;
    @SerializedName("releases_url")
    private String releasesUrl;
    private Long size;
    @SerializedName("ssh_url")
    private String sshUrl;
    @SerializedName("stargazers_count")
    private Long stargazersCount;
    @SerializedName("stargazers_url")
    private String stargazersUrl;
    @SerializedName("statuses_url")
    private String statusesUrl;
    @SerializedName("subscribers_url")
    private String subscribersUrl;
    @SerializedName("subscription_url")
    private String subscriptionUrl;
    @SerializedName("svn_url")
    private String svnUrl;
    @SerializedName("tags_url")
    private String tagsUrl;
    @SerializedName("teams_url")
    private String teamsUrl;
    @SerializedName("trees_url")
    private String treesUrl;
    @SerializedName("updated_at")
    private String updatedAt;
    private String url;
    private Long watchers;
    @SerializedName("watchers_count")
    private Long watchersCount;
}
