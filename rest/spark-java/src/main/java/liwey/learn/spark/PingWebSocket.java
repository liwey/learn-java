/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/11 16:54
 */

package liwey.learn.spark;


import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.io.IOException;

@WebSocket
public class PingWebSocket {
    @OnWebSocketConnect
    public void connected(Session session) throws IOException {
        int port = session.getRemote().getInetSocketAddress().getPort();
        System.out.println("连接建立" + port);
        session.getRemote().sendString("Your port is " + port);
    }

    @OnWebSocketClose
    public void closed(Session session, int statusCode, String reason) {
        System.out.println("Session " + session + " closed (" +  reason + ", code = "+ statusCode + ")");
    }

    @OnWebSocketMessage
    public void message(Session session, String message) throws IOException {
        int port = session.getRemote().getInetSocketAddress().getPort();
        System.out.println("Got " + port + ": " + message);
        session.getRemote().sendString("Received " + message.toLowerCase());
    }
}