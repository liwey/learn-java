package com.tutorial.interceptor;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    /**
     * @param args
     */
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("intercept.xml");
        Interface bean1 = (Interface) context.getBean("bean1");
        bean1.hello();

        Interface bean2 = (Interface) context.getBean("bean2");
        bean2.hello();
    }
}
