package com.tutorial.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class ToJsonStringAspect {
    @Around("toJsonString()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        //Object result = pjp.proceed();
        return pjp.getThis().toString();
    }

    @Pointcut("execution(* com.tutorial.aop.*.toString(..))")
    private void toJsonString() {
    }
}
