package com.tutorial.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class Aspect2 {
    @Pointcut("execution(* com.tutorial.aop.Student.getName(..))")
    private void getName() {}

    @After("getName()")
    public void beforeAdvice(JoinPoint point){
        System.out.println("before method." + point.getSignature());
    }
}
