package com.tutorial.aop;

public class ToJson {
    public static <T> String toJsonString(T obj){
        return "json of " + obj.getClass();
    }
}
