package com.tutorial.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class Aspect4 {
    @Around("point1()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println(pjp.getSignature().getName() + " begin...");
        Object result = pjp.proceed();
        System.out.println(pjp.getSignature().getName() + " end...");
        return result;
    }

    @Pointcut("execution(* com.tutorial.aop.Student.getName(..))")
    private void point1() {
    }
}