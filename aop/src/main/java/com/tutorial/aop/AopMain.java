package com.tutorial.aop;

import lombok.experimental.ExtensionMethod;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@ExtensionMethod(ToJson.class)
public class AopMain {
    public static void main(String[] args) {
        Student s = new Student();
        new AopMain().test();
    }

    private void test() {
        ApplicationContext context = new ClassPathXmlApplicationContext("com/tutorial/aop/aop2.xml");
        Student student1 = (Student) context.getBean("student");
        System.out.println(student1.toString());
        Student student2 = new Student(11, "Zara");
        System.out.println(student2.toString());
    }

    private String exec(String command) {
        StringBuffer output = new StringBuffer();
        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            output.append("Exit code = " + p.waitFor());
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.toString();
    }
}