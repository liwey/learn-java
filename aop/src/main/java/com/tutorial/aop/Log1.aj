package com.tutorial.aop;

public aspect Log1 {
    private int callDepth;

    private pointcut executionJoinPoints(): !within(Log1) && execution (* *.*(..));

    before(): executionJoinPoints(){
        print("Before call " + thisJoinPoint);
        callDepth++;
    }

    after(): executionJoinPoints(){
        callDepth--;
        print("After call " + thisJoinPoint);
    }

    private void print(String s){
        for(int i=0; i<callDepth; i++)
            System.out.print("    ");
        System.out.println(s);
    }
}
