package com.tutorial.aop;

import lombok.*;
import lombok.experimental.ExtensionMethod;

@Data @NoArgsConstructor @AllArgsConstructor
@ExtensionMethod(ToJson.class)
public class Student {
    private Integer age;
    private String name;

    public void printThrowException(){
        System.out.println("Exception raised");
        throw new IllegalArgumentException();
    }
}