package com.tutorial.aop;

import org.aspectj.lang.JoinPoint;

import java.io.Serializable;

public class Aspect1 implements Serializable{
    /**
     * This is the method which I would like to execute
     * before a selected method execution.
     */
    public void beforeAdvice(JoinPoint point){
        System.out.println("before method." + point.getSignature());
    }
    /**
     * This is the method which I would like to execute
     * after a selected method execution.
     */
    public void afterAdvice(){
        System.out.println("after method.");
    }
    /**
     * This is the method which I would like to execute
     * when any method returns.
     */
    public void afterReturningAdvice(Object retVal){
        System.out.println("returned:" + retVal.toString() );
    }
    /**
     * This is the method which I would like to execute
     * if there is an exception raised.
     */
    public void AfterThrowingAdvice(IllegalArgumentException ex){
        System.out.println("There has been an exception: " + ex.toString());
    }
}