package com.tutorial.orm;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class Users {
    private int id;
    private String email;
    private String ip;
    private String os;
    private Timestamp created;
    private Timestamp updated;
    private String lang;
    private Integer timezone;
}
