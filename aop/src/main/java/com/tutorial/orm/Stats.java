package com.tutorial.orm;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class Stats {
    private Timestamp time;
    private String ki;
    private Integer count;
}
