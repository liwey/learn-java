package com.tutorial.orm;

import lombok.Data;

@Data
public class Trades {
    private int id;
    private String name;
}
