package com.tutorial.orm;

public interface KsDao {
    public String findBookById(int id);
    public void saveBook(Remaps remaps);
}