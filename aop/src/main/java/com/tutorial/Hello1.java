package com.tutorial;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Hello1 extends HelloWorld {
    private String message1;
}
