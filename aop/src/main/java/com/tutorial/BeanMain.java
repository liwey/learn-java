package com.tutorial;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class BeanMain {

    public static void main(String[] args) {
        new BeanMain().test();
    }

    ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

    private void test(){
        //context.start();
        HelloWorld obj = (HelloWorld) context.getBean("hello2");
        System.out.println(obj);
        //context.registerShutdownHook();
        TextEditor te = (TextEditor) context.getBean("textEditor");
        te.spellCheck();
        //context.stop();

        AbstractApplicationContext ctx =
                new AnnotationConfigApplicationContext(TextEditorConfig.class);
        ctx.start();
        TextEditor te2 = ctx.getBean(TextEditor.class);
        te2.spellCheck();
        ctx.stop();
    }
}