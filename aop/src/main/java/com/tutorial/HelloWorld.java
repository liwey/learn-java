package com.tutorial;

import lombok.Data;

@Data
public class HelloWorld {
    private String message;

    public void init(){
        System.out.println("init() called.");
    }

    public void destroy(){
        System.out.println("destroy() called.");
    }
}