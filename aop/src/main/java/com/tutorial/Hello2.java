package com.tutorial;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class Hello2 extends HelloWorld {
    private String message2;
}
