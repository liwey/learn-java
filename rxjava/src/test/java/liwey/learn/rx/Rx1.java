/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/6 10:32
 */

package liwey.learn.rx;


import junit.framework.TestCase;
import rx.Observable;
import rx.Subscriber;

/**
 *
 */
public class Rx1 extends TestCase{
    public void test1(){
        Integer[] integers = {-1, 4,5,0,2,19,6};
        Observable<Integer> workflow = Observable.from(integers)
              .filter(i -> (i < 10) && (i > 0))
              .map(i -> i * 2);
        workflow.subscribe(i -> System.out.print(i + "! "));
    }

    public void test2() {
        String[] names = {"str1","str2","str2","str3"};

        Observable.from(names).subscribe(new Subscriber<String>() {
            @Override
            public void onCompleted() {
                System.out.println("Completed!");
            }

            @Override
            public void onError(Throwable throwable) {
                throwable.printStackTrace();
            }

            @Override
            public void onNext(String str) {
                System.out.println("same hello " + str);
            }
        });
    }

}
