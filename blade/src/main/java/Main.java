/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/5/10 18:50
 */

import com.blade.Blade;

/**
 *
 */
public class Main {
    public static void main(String[] args) {
        Blade.me().listen(9001).get("/h", (req, res) -> {
            res.text("Hello");
        }).get("/",(r,o)->{
            o.text("abc");
        }).start();
    }
}
