package threads;

public class MyRunnable implements Runnable {
    private String anyData;

    public MyRunnable() {
    }

    public MyRunnable(String anyData) {
        this.anyData = anyData;
    }

    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("[" + Thread.currentThread().getName() + "] " + "Message " + i);
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
