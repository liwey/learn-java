package threads;

import lombok.NonNull;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Set<Callable<String>> callableSet = new HashSet<>();
        callableSet.add(() -> "Task 1");
        callableSet.add(() -> "Task 2");
        callableSet.add(() -> "Task 3");
        String result = executorService.invokeAny(callableSet);
        System.out.println("result = " + result);

        Future<String> future = executorService.submit((Callable) () -> {
            System.out.println("Asynchronous Callable");
            Thread.sleep(5000);
            return "Callable Result";
        });

        executorService.shutdown();
        System.out.println("finished.");
        System.out.println("future.get() = " + future.get());
    }

    private static int add(Integer a, Integer b){
        return a + b;
    }
}
