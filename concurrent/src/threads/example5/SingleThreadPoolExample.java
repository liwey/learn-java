package threads.example5;

import threads.MyRunnable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * Instantiates a thread pool with a single thread
 * All tasks are executed sequentially by the same thread
 */

public class SingleThreadPoolExample {
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		
		System.out.println("Thread main started");
		
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.execute(new MyRunnable());
		executorService.execute(new MyRunnable());
		executorService.execute(new MyRunnable());
		executorService.execute(new MyRunnable());
		executorService.execute(new MyRunnable());
		
		executorService.shutdown();
		
		System.out.println("Thread main finished");
	}
}
