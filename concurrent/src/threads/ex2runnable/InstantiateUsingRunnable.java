package threads.ex2runnable;

/*
 * Instantiate a thread by using Runnable interface
 */

import threads.MyRunnable;

public class InstantiateUsingRunnable {
	
	public static void main(String[] args) {
		
		System.out.println("Thread main started");
		
		Thread thread1 = new Thread(new MyRunnable("any data that the thread may need to process"));
		Thread thread2 = new Thread(new MyRunnable("any data that the thread may need to process"));
		thread1.start();
		thread2.start();
		
		System.out.println("Thread main finished");
	}
}
