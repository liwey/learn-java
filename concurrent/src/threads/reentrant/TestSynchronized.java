/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/8 16:47
 */

package threads.reentrant;

import sun.misc.Unsafe;

import static threads.reentrant.TestSynchronized.lock1;

/**
 *
 */
public class TestSynchronized {
    private static synchronized void method1() {
        System.out.println("in method1");
    }

    private static synchronized void method2() {
        method1();
    }

    public static void main(String[] args) {
        synchronized (lock1) {
            Th1 th1 = new Th1();
            th1.start();
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("main done");
            lock1.notify();
        }
    }


    static Object lock1 = new Object();
}

class Th1 extends Thread {

    @Override
    public synchronized void run() {
        System.out.println("th1");
        try {
            lock1.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("step 1 done");
        lock1.notify();
    }
}


