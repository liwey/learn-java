grammar Exp;

prog : expression EOF;

//case words
IF : 'if';
THEN : 'then';
ELSE : 'else';
SWITCH : 'switch';
CASE : 'case';
DEFAULT :'default';

expression : leafExpression | numExpression | function | statement | LPAREN expression RPAREN;

//expression which has no children
leafExpression : NUMBER | TF | STRING | parameter | column | constant ;

boolExpression: orExpression;
orExpression :andExpression (OR andExpression )+;
andExpression : TF | compareExpression (AND compareExpression)+ ;
notExpression: NOT boolExpression;

compareExpression : (numExpression | STRING) (EQ | NE | LT | LE | GT | GE) (numExpression | STRING);

numExpression: addExpression;
addExpression : NUMBER ((ADD | SUB) NUMBER)+;
mulExpression : NUMBER ((MUL | DIV | MOD) NUMBER )+;
powerExpression : NUMBER POW NUMBER;

primaryExpression : statement | LPAREN orExpression RPAREN | leafExpression;

TF : 'true' | 'false' ;

// Must be declared after the BOOLEAN token or it will hide it
function : IDENTIFIER LPAREN (expression (COMMA expression)* )? RPAREN ;

statement : if_statement | case_statement;
if_statement : IF LPAREN boolExpression RPAREN THEN primaryExpression ELSE primaryExpression;
case_statement : SWITCH LPAREN expression RPAREN LBRACE (CASE leafExpression COLON expression SEMI )+ (DEFAULT COLON expression)?
RBRACE;

parameter : LBRACK IDENTIFIER RBRACK;
column : LBRACE IDENTIFIER RBRACE;
constant : time_unit | 'pi';
time_unit : 'year' | 'month' | 'day' ;
NUMBER: ('0'..'9')+ ('.' ('0'..'9')+)?;
STRING : '"' ( EscapeSequence | ~('\u0000'..'\u001f' | '\\' | '"' ))* '"';
fragment EscapeSequence : '\\'('n' | 'r' | 't' | '"' | '\\' | UnicodeEscape);
fragment UnicodeEscape : 'u' HexDigit HexDigit HexDigit HexDigit;
fragment HexDigit : ('0'..'9' | 'a'..'f' | 'A'..'F') ;

IDENTIFIER : ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*;

LPAREN : '(';
RPAREN : ')';
COMMA : ',';

POW : '^';
MUL : '*';
DIV : '/';
MOD : '%';
ADD : '+';
SUB : '-';

EQ : '=' | '==';
NE : '!=' | '<>';
LT : '<';
LE : '<=';
GT : '>';
GE : '>=';

AND : '&&' | 'and';
OR : '||' | 'or';
NOT : '!' | 'not';

LBRACE : '{';
RBRACE : '}';
LBRACK : '[';
RBRACK : ']';
SEMI : ';';
COLON : ':';

/* Ignore white spaces */
WS : (' ' | '\r' | '\t' | '\u000C' | '\n') -> skip;
