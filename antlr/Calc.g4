grammar Calc;

prog : expression EOF;

IF : 'if';
THEN : 'then';
ELSE : 'else';
SWITCH : 'switch';
CASE : 'case';
DEFAULT :'default';

expression : orExpression;
 
orExpression : andExpression (OR andExpression )* ;

andExpression : compareExpression (AND compareExpression)* ;

compareExpression : addExpression ((EQ | NE | LT | LE | GT | GE) addExpression)*;

addExpression : mulExpression ( (ADD | SUB) mulExpression )*;

mulExpression : powerExpression ( (MUL | DIV | MOD) powerExpression )* ;

powerExpression : unaryExpression ( POW unaryExpression )* ;

unaryExpression : primaryExpression | NOT primaryExpression | SUB primaryExpression;

primaryExpression : statement | LPAREN orExpression RPAREN | function | value;

BOOLEAN : 'true' | 'false' ;

// Must be declared after the BOOLEAN token or it will hide it
function : IDENTIFIER LPAREN (expression (COMMA expression)* )? RPAREN ;

statement : if_statement | case_statement;
if_statement : IF '(' expression ')' THEN primaryExpression ELSE primaryExpression;
case_statement : SWITCH LPAREN expression RPAREN LBRACE (CASE value COLON expression SEMI )+ (DEFAULT COLON expression)?
RBRACE;

value : INTEGER | FLOAT | DATETIME | BOOLEAN | STRING | parameter | column | constant ;

parameter : '[' IDENTIFIER ']' ;

column : '{' IDENTIFIER '}';

constant : time_unit | 'pi';

time_unit : 'year' | 'month' | 'day' ;

STRING : '"' ( EscapeSequence | ~('\u0000'..'\u001f' | '\\' | '"' ))* '"';

INTEGER : ('0'..'9')+ ;

FLOAT : ('0'..'9')* '.' ('0'..'9')+ ;

DATETIME : '#' (~'#')* '#';


LPAREN : '(';
RPAREN : ')';
COMMA : ',';


IDENTIFIER : ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*;

fragment EscapeSequence : '\\'('n' | 'r' | 't' | '"' | '\\' | UnicodeEscape);

fragment UnicodeEscape : 'u' HexDigit HexDigit HexDigit HexDigit;

fragment HexDigit : ('0'..'9' | 'a'..'f' | 'A'..'F') ;

/* Ignore white spaces */ 
WS : (' ' | '\r' | '\t' | '\u000C' | '\n') -> skip;


// Literals

DECIMAL_LITERAL : ('0' | [1-9] (Digits? | '_'+ Digits)) [lL]?;
HEX_LITERAL : '0' [xX] [0-9a-fA-F] ([0-9a-fA-F_]* [0-9a-fA-F])? [lL]?;
OCT_LITERAL : '0' '_'* [0-7] ([0-7_]* [0-7])? [lL]?;
BINARY_LITERAL : '0' [bB] [01] ([01_]* [01])? [lL]?;

FLOAT_LITERAL : (Digits '.' Digits? | '.' Digits) ;

BOOL_LITERAL : 'true' | 'false'
 ;

CHAR_LITERAL : '\'' (~['\\\r\n] | EscapeSequence) '\'';

STRING_LITERAL : '"' (~["\\\r\n] | EscapeSequence)* '"';
NULL_LITERAL : 'null';


MUL : '*';
DIV : '/';
MOD : '%';
ADD : '+';
SUB : '-';
POW : '^';

EQ : '=' | '==';
NE : '!=' | '<>';
LT : '<';
LE : '<=';
GT : '>';
GE : '>=';

AND : '&&' | 'and';
OR : ' | | ' | 'or';
NOT : '!' | 'not';


LBRACE : '{';
RBRACE : '}';
LBRACK : '[';
RBRACK : ']';
SEMI : ';';
DOT : '.';
// Operators
ASSIGN : '=';
QUESTION : '?';
COLON : ':';
EQUAL : '==';
NOTEQUAL : '!=';
INC : '++';
DEC : '--';
BITAND : '&';
BITOR : ' | ';
CARET : '^';

fragment Digits
 : [0-9] ([0-9_]* [0-9])?
 ;