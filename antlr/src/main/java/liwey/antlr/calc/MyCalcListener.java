/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/17 20:16
 */

package liwey.antlr.calc;

import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class MyCalcListener extends CalcBaseListener {
    @Override public void enterFunction(CalcParser.FunctionContext ctx) {
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation does nothing.</p>
     */
    @Override public void exitFunction(CalcParser.FunctionContext ctx) {
        if(!funs.contains(ctx.IDENTIFIER().toString())){
            System.out.println("invalid function name " + ctx.IDENTIFIER().toString());
        }
        if(ctx.getChildCount() < 3){
            System.out.println("invalid arg count");
        }
    }

    @Override public void exitIf_statement(CalcParser.If_statementContext ctx) {
        System.out.println(ctx.getChildCount());
    }

    List<String> funs = Arrays.asList("avg","concat");
    List<Integer> argn = Arrays.asList(5,8);
}
