// Generated from D:/scratch/llian/workspaces/learn/antlr\Calc.g4 by ANTLR 4.7
package liwey.antlr.calc;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link CalcParser}.
 */
public interface CalcListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link CalcParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(CalcParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(CalcParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(CalcParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(CalcParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#orExpression}.
	 * @param ctx the parse tree
	 */
	void enterOrExpression(CalcParser.OrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#orExpression}.
	 * @param ctx the parse tree
	 */
	void exitOrExpression(CalcParser.OrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#andExpression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpression(CalcParser.AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#andExpression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpression(CalcParser.AndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#compareExpression}.
	 * @param ctx the parse tree
	 */
	void enterCompareExpression(CalcParser.CompareExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#compareExpression}.
	 * @param ctx the parse tree
	 */
	void exitCompareExpression(CalcParser.CompareExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#addExpression}.
	 * @param ctx the parse tree
	 */
	void enterAddExpression(CalcParser.AddExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#addExpression}.
	 * @param ctx the parse tree
	 */
	void exitAddExpression(CalcParser.AddExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#mulExpression}.
	 * @param ctx the parse tree
	 */
	void enterMulExpression(CalcParser.MulExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#mulExpression}.
	 * @param ctx the parse tree
	 */
	void exitMulExpression(CalcParser.MulExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#powerExpression}.
	 * @param ctx the parse tree
	 */
	void enterPowerExpression(CalcParser.PowerExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#powerExpression}.
	 * @param ctx the parse tree
	 */
	void exitPowerExpression(CalcParser.PowerExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpression(CalcParser.UnaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#unaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpression(CalcParser.UnaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryExpression(CalcParser.PrimaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#primaryExpression}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryExpression(CalcParser.PrimaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(CalcParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(CalcParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(CalcParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(CalcParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement(CalcParser.If_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement(CalcParser.If_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#case_statement}.
	 * @param ctx the parse tree
	 */
	void enterCase_statement(CalcParser.Case_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#case_statement}.
	 * @param ctx the parse tree
	 */
	void exitCase_statement(CalcParser.Case_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(CalcParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(CalcParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(CalcParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(CalcParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#column}.
	 * @param ctx the parse tree
	 */
	void enterColumn(CalcParser.ColumnContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#column}.
	 * @param ctx the parse tree
	 */
	void exitColumn(CalcParser.ColumnContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(CalcParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(CalcParser.ConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link CalcParser#time_unit}.
	 * @param ctx the parse tree
	 */
	void enterTime_unit(CalcParser.Time_unitContext ctx);
	/**
	 * Exit a parse tree produced by {@link CalcParser#time_unit}.
	 * @param ctx the parse tree
	 */
	void exitTime_unit(CalcParser.Time_unitContext ctx);
}