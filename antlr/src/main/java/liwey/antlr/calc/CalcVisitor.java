// Generated from D:/scratch/llian/workspaces/learn/antlr\Calc.g4 by ANTLR 4.7
package liwey.antlr.calc;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link CalcParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface CalcVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link CalcParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(CalcParser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(CalcParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#orExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrExpression(CalcParser.OrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#andExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndExpression(CalcParser.AndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#compareExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompareExpression(CalcParser.CompareExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#addExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAddExpression(CalcParser.AddExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#mulExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMulExpression(CalcParser.MulExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#powerExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPowerExpression(CalcParser.PowerExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#unaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryExpression(CalcParser.UnaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#primaryExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryExpression(CalcParser.PrimaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(CalcParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(CalcParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#if_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_statement(CalcParser.If_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#case_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCase_statement(CalcParser.Case_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(CalcParser.ValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter(CalcParser.ParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#column}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitColumn(CalcParser.ColumnContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant(CalcParser.ConstantContext ctx);
	/**
	 * Visit a parse tree produced by {@link CalcParser#time_unit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTime_unit(CalcParser.Time_unitContext ctx);
}