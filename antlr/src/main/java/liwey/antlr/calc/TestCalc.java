/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/17 20:19
 */

package liwey.antlr.calc;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;

/**
 *
 */
public class TestCalc {
    public static void main(String[] args) throws IOException {
        String exp = "if(2)then 1 else 0";
        CalcLexer lexer = new CalcLexer(CharStreams.fromString(exp));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CalcParser parser = new CalcParser(tokens);
        parser.addParseListener(new MyCalcListener());
        ParseTree tree = parser.prog();
        System.out.println(tree.toStringTree(parser));
    }
}
