package liwey.learn.shuntingyard;

import java.util.Scanner;
import java.util.NoSuchElementException;

public class Main {
    public static void main(String[] args) {
	    Scanner sc = new Scanner((System.in));
	    while(true){
		    System.out.print("Insert an expression: ");
            String expression = sc.nextLine();
            if(expression.isEmpty())
                continue;
            if(expression.equals("e"))
                return;

            try {
                System.out.println(new RpnEvaluator(
                        new ShuntingYardAlgorithm(expression).toRpn()).evaluate());
            } catch (NoSuchElementException e) {
                System.out.println("Syntax error!");
            }
	    }
    }
}
