/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/6 23:27
 */

package liwey.learn.basic;

import java.util.Stack;

/**
 *
 */
public class ExpParser {
    private Stack<String> operators = new Stack<>();
    private Stack<String> output = new Stack<>();

    private void parse(String[] tokens) throws Exception {
        operators.clear();
        output.clear();

        for (String token : tokens) {
            //if the token is a value, then push it to the output queue.
            if (isValue(token)) {
                output.push(token);
                continue;
            }

            //	if the token is a left bracket (i.e. "("), then push it onto the operator stack.
            if (token.equals("(")) {
                operators.push(token);
                continue;
            }
            // if the token is a function then push it onto the operator stack
            if(isFunction(token)){
                operators.push(token);
                continue;
            }
            //if the token is a right bracket (i.e. ")"), then:
            //while the operator at the top of the operator stack is not a left bracket:
            //pop the operator from the operator stack onto the output queue.
            //      pop the left bracket from the stack.
            if (token.equals(")")) {
                String op = operators.pop();
                while (!op.equals("(")){
                    if (operators.isEmpty())
                        throw new Exception("imbalanced");
                    output.push(op);
                    op = operators.pop();
                }
                continue;
            }

            //	if the token is an operator, then:
            //		while ((there is a function at the top of the operator stack)
            //		       or (there is an operator at the top of the operator stack with greater precedence)
            //		       or (the operator at the top of the operator stack has equal precedence and is left associative))
            //		      and (the operator at the top of the operator stack is not a left bracket):
            //			pop operators from the operator stack onto the output queue.
            //		push it onto the operator stack.
            if(!operators.empty()) {
                String leftOp = operators.peek();
                while (isFunction(leftOp) || precedence(leftOp) >= precedence(token) && !leftOp.equals("(")) {
                    String op = operators.pop();
                    output.push(op);
                    leftOp = operators.peek();
                }
            }
            operators.push(token);
        }

        //if there are no more tokens to read:
        //	while there are still operator tokens on the stack:
        //		/* if the operator token on the top of the stack is a bracket, then there are mismatched parentheses. */
        //		pop the operator from the operator stack onto the output queue.
        while (!operators.isEmpty()) {
            String op = operators.pop();
            if (op.equals(")"))
                throw new Exception("there is a )");
            output.push(op);
        }
    }

    private boolean isFunction(String token) {
        switch (token){
            case "sin":
            case "max":
                return true;
        }
        return false;
    }

    public static void main(String[] args) throws Exception {
        //3 + 4 × 2 ÷ ( 1 − 5 ) ^ 2
        ExpParser parser = new ExpParser();

        //3 + 4 × 2 ÷ ( 1 − 5 ) ^ 2
        String[] tokens = {"3", "+", "4", "*", "2", "/", "(", "1", "-", "5", ")", "^", "2"};
        parser.parse(tokens );
        //3 4 2 * 1 5 − 2  ^  / +

        //sin ( max ( 2, 3 ) ÷ 3 × 3.1415 )
        String[] tokens2 = {"sin", "(", "max", "(", "2", "3", ")", "÷", "3", "×", "3.1415", ")"};
        parser.parse(tokens2);
        //2 3 max 3 ÷ 3.1415 × sin
    }

    private int precedence(String op) {
        switch (op) {
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
            case "×":
            case "÷":
                return 2;
            case "^":
                return 3;
        }
        return 0;
    }


    private boolean isValue(String token) {
        try {
            Double.parseDouble(token);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

