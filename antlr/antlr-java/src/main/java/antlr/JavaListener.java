// Generated from D:/scratch/llian/workspaces/learn/antlr/antlr-java\Java.g4 by ANTLR 4.7
package antlr;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link JavaParser}.
 */
public interface JavaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link JavaParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(JavaParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(JavaParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by the {@code int}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterInt(JavaParser.IntContext ctx);
	/**
	 * Exit a parse tree produced by the {@code int}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitInt(JavaParser.IntContext ctx);
	/**
	 * Enter a parse tree produced by the {@code float}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterFloat(JavaParser.FloatContext ctx);
	/**
	 * Exit a parse tree produced by the {@code float}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitFloat(JavaParser.FloatContext ctx);
	/**
	 * Enter a parse tree produced by the {@code char}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterChar(JavaParser.CharContext ctx);
	/**
	 * Exit a parse tree produced by the {@code char}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitChar(JavaParser.CharContext ctx);
	/**
	 * Enter a parse tree produced by the {@code str}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterStr(JavaParser.StrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code str}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitStr(JavaParser.StrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code bool}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterBool(JavaParser.BoolContext ctx);
	/**
	 * Exit a parse tree produced by the {@code bool}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitBool(JavaParser.BoolContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nul}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterNul(JavaParser.NulContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nul}
	 * labeled alternative in {@link JavaParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitNul(JavaParser.NulContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(JavaParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(JavaParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaParser#caseStatement}.
	 * @param ctx the parse tree
	 */
	void enterCaseStatement(JavaParser.CaseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaParser#caseStatement}.
	 * @param ctx the parse tree
	 */
	void exitCaseStatement(JavaParser.CaseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaParser#caseItem}.
	 * @param ctx the parse tree
	 */
	void enterCaseItem(JavaParser.CaseItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaParser#caseItem}.
	 * @param ctx the parse tree
	 */
	void exitCaseItem(JavaParser.CaseItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaParser#defaultItem}.
	 * @param ctx the parse tree
	 */
	void enterDefaultItem(JavaParser.DefaultItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaParser#defaultItem}.
	 * @param ctx the parse tree
	 */
	void exitDefaultItem(JavaParser.DefaultItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaParser#parExpression}.
	 * @param ctx the parse tree
	 */
	void enterParExpression(JavaParser.ParExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaParser#parExpression}.
	 * @param ctx the parse tree
	 */
	void exitParExpression(JavaParser.ParExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void enterExpressionList(JavaParser.ExpressionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void exitExpressionList(JavaParser.ExpressionListContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaParser#constantExpression}.
	 * @param ctx the parse tree
	 */
	void enterConstantExpression(JavaParser.ConstantExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaParser#constantExpression}.
	 * @param ctx the parse tree
	 */
	void exitConstantExpression(JavaParser.ConstantExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tuple}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterTuple(JavaParser.TupleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tuple}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitTuple(JavaParser.TupleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code prim}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPrim(JavaParser.PrimContext ctx);
	/**
	 * Exit a parse tree produced by the {@code prim}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPrim(JavaParser.PrimContext ctx);
	/**
	 * Enter a parse tree produced by the {@code not}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNot(JavaParser.NotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code not}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNot(JavaParser.NotContext ctx);
	/**
	 * Enter a parse tree produced by the {@code or}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterOr(JavaParser.OrContext ctx);
	/**
	 * Exit a parse tree produced by the {@code or}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitOr(JavaParser.OrContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifStmt}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIfStmt(JavaParser.IfStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifStmt}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIfStmt(JavaParser.IfStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code and}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAnd(JavaParser.AndContext ctx);
	/**
	 * Exit a parse tree produced by the {@code and}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAnd(JavaParser.AndContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ltgt}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLtgt(JavaParser.LtgtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ltgt}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLtgt(JavaParser.LtgtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code addSub}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAddSub(JavaParser.AddSubContext ctx);
	/**
	 * Exit a parse tree produced by the {@code addSub}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAddSub(JavaParser.AddSubContext ctx);
	/**
	 * Enter a parse tree produced by the {@code power}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPower(JavaParser.PowerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code power}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPower(JavaParser.PowerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code caseStmt}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCaseStmt(JavaParser.CaseStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code caseStmt}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCaseStmt(JavaParser.CaseStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code eqne}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEqne(JavaParser.EqneContext ctx);
	/**
	 * Exit a parse tree produced by the {@code eqne}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEqne(JavaParser.EqneContext ctx);
	/**
	 * Enter a parse tree produced by the {@code mulDiv}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMulDiv(JavaParser.MulDivContext ctx);
	/**
	 * Exit a parse tree produced by the {@code mulDiv}
	 * labeled alternative in {@link JavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMulDiv(JavaParser.MulDivContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parExp}
	 * labeled alternative in {@link JavaParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterParExp(JavaParser.ParExpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parExp}
	 * labeled alternative in {@link JavaParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitParExp(JavaParser.ParExpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lit}
	 * labeled alternative in {@link JavaParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterLit(JavaParser.LitContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lit}
	 * labeled alternative in {@link JavaParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitLit(JavaParser.LitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code id}
	 * labeled alternative in {@link JavaParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterId(JavaParser.IdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code id}
	 * labeled alternative in {@link JavaParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitId(JavaParser.IdContext ctx);
	/**
	 * Enter a parse tree produced by the {@code fun}
	 * labeled alternative in {@link JavaParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterFun(JavaParser.FunContext ctx);
	/**
	 * Exit a parse tree produced by the {@code fun}
	 * labeled alternative in {@link JavaParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitFun(JavaParser.FunContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(JavaParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(JavaParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link JavaParser#arguments}.
	 * @param ctx the parse tree
	 */
	void enterArguments(JavaParser.ArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link JavaParser#arguments}.
	 * @param ctx the parse tree
	 */
	void exitArguments(JavaParser.ArgumentsContext ctx);
}