/*
 * Copyright
 *
 * @author      Leon Zeng
 * @version     1.0
 * Created       2018/4/18 11:37
 */

import antlr.JavaLexer;
import antlr.JavaParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;

/**
 *
 */
public class JavaMain {
    public static void main(String[] args) throws IOException {
        String exp = "fun1(a,b,c)";
        JavaLexer lexer = new JavaLexer(CharStreams.fromString(exp));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        JavaParser parser = new JavaParser(tokens);
        parser.addParseListener(new MyJavaListener());
        ParseTree tree = parser.expression();
        System.out.println(tree.toStringTree(parser));
    }
}
