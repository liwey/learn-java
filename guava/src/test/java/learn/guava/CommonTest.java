package learn.guava;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import org.junit.Test;

import java.util.*;

public class CommonTest {
    @Test
    public void testOptional() {
        Integer value1 = null;
        Integer value2 = 10;
        Optional<Integer> a = Optional.ofNullable(value1);
        Optional<Integer> b = Optional.of(value2); //返回包含给定的非空引用Optional实例
        System.out.println(sum(a, b));
    }

    private static Integer sum(Optional<Integer> a, Optional<Integer> b) {
        //isPresent():如果Optional包含非null的引用（引用存在），返回true
        System.out.println("First param is present: " + a.isPresent());
        System.out.println("Second param is present: " + b.isPresent());
        Integer value1 = a.orElse(0);  //返回Optional所包含的引用,若引用缺失,返回指定的值
        Integer value2 = b.get(); //返回所包含的实例,它必须存在,通常在调用该方法时会调用isPresent()判断是否为null
        return value1 + value2;
    }


    @Test
    public void testPreconditions() {
        try {
            getValue(5);
        } catch (IndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }

        try {
            sum(4, null);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }

        try {
            sqrt(-1);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }

    }

    private static double sqrt(double input) {
        Preconditions.checkArgument(input > 0.0,
                "Illegal Argument passed: Negative value %s.", input);
        return Math.sqrt(input);
    }

    private static int sum(Integer a, Integer b) {
        a = Preconditions.checkNotNull(a, "Illegal Argument passed: First parameter is Null.");
        b = Preconditions.checkNotNull(b, "Illegal Argument passed: First parameter is Null.");
        return a + b;
    }

    private static int getValue(int input) {
        int[] data = {1, 2, 3, 4, 5};
        int index = Preconditions.checkElementIndex(input, data.length, "Illegal Argument passed: Invalid index.");
        return data[index];
    }


    @Test
    public void testJoiner() {
        StringBuilder sb = new StringBuilder();
        Joiner.on(",").skipNulls().appendTo(sb, "Hello", "guava");
        System.out.println(sb);
        System.out.println(Joiner.on(",").useForNull("none").join(1, null, 3));
        System.out.println(Joiner.on(",").skipNulls().join(Arrays.asList(1, 2, 3, 4, null, 6)));
        Map<String, String> map = new HashMap<>();
        map.put("key1", "value1");
        map.put("key2", "value2");
        map.put("key3", "value3");
        System.out.println(Joiner.on(",").withKeyValueSeparator("=").join(map));
    }

    @Test
    public void testSplitter() {
        Iterable a = Splitter.on(",").limit(5).trimResults().split(" a,  b,  c,  d,    e, f,g");//[ a, b, c,d]
        Iterator i = a.iterator();
        while(i.hasNext()){
            System.out.println(i.next());
        }
        Iterable b = Splitter.fixedLength(3).split("1 2 3");//[1 2,  3]
        List c = Splitter.on(" ").omitEmptyStrings().splitToList("1  2 3");
        Iterable d = Splitter.on(",").omitEmptyStrings().split("1,,,,2,,,3");//[1, 2, 3]
        Iterable e = Splitter.on(" ").trimResults().split("1 2 3"); //[1, 2, 3],默认的连接符是,
        Map f = Splitter.on(";").withKeyValueSeparator(":").split("a:1;b:2;c:3");//{a=1, b=2, c=3}
    }
}


