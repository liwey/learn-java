package learn.guava;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;

public class CollectionsTest {
    @Test
    public void testNew() {
        ArrayList<String> l1 = Lists.newArrayList();
        HashSet<String> l2 = Sets.newHashSet();
        l2.add("a");
    }

    @Test
    public void testImmutable() {
        ImmutableList<Integer> l1 = ImmutableList.of(1,2,3);
        ImmutableSet<Integer> s1 = ImmutableSet.of(1,1,2,3);
        l1.contains(1);
    }
}
