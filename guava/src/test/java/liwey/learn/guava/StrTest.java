/**
 * Copyright
 *
 * @author Leon Zeng
 * @version 1.0
 * Created       2018/3/31 15:34
 */

package liwey.learn.guava;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

import junit.framework.TestCase;
import org.apache.commons.lang3.StringUtils;

/**
 *
 */
public class StrTest extends TestCase {
    public void testSplitSpeed() {
        final String numberList = "One,Two,Three,Four,Five,Six,Seven,Eight,Nine,Ten";

        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            Splitter.on(',').split(numberList);
        }
        System.out.println(System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            StringUtils.split(numberList, ',');
        }
        System.out.println(System.currentTimeMillis() - start);
    }

    public void testJoinSpeed() {
        final String[] numberList = new String[]{"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"};

        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            Joiner.on(',').join(numberList);
        }
        System.out.println(System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        for (int i = 0; i < 1000000; i++) {
            StringUtils.join(numberList, ',');
        }
        System.out.println(System.currentTimeMillis() - start);
    }
}
